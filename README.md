# mvi-sp

# Multilanguage name matching

Ability to match same names despite the script, language or transcription used is an important task in many fields - not least in crime detection and compliance. For example following pairs represent the same name: Борис Ельцин vs Boris Yeltsin or Osama bin Laden vs أسامة بن محمد بن عوض بن لادن‎.
Your task is to build a deep learning model for matching names written in different languages and scripts. One possible approach is to build a name vector representation (embedding model) and measure the name similarity by distance between the latent representation but any other approach is welcome.

## Overview

1. [Multilingual Dual-Encoder Model](https://gitlab.fit.cvut.cz/hotlimyk/mvi-sp/blob/master/multilingual_dual_encoder.ipynb) to train multilingual sentece embeddings.

2. Report [report.pdf](https://gitlab.fit.cvut.cz/hotlimyk/mvi-sp/blob/master/report.pdf)

3. https://gitlab.fit.cvut.cz/hotlimyk/mvi-sp/blob/master/load_data.ipynb to create train.csv/val.csv/test.csv data set + source.txt/target.txt to train the charater tokenizer.


## Dependencies

[sentencepiece](https://github.com/google/sentencepiece): `pip install sentencepiece` (tokenizer)

## How to run.

1. All data has already been prepared, but you can use load_data.ipynb to resize the size of training set.

2. Run each cell in **multilingual_dual_encoder.ipynb** to train the model and get the result..
